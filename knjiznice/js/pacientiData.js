var pacienti = {
    
1: {
        data: {
        firstNames: "Marko",
        lastNames: "Bolanko",
        dateOfBirth: "1968-01-10T12:22",
    },
    
    party: {                    		    
        1: {
        "ctx/language": "en",
	    "ctx/territory": "SI",
	    "ctx/time": "2014-03-03T07:28:33.000+01:00",
	    "vital_signs/blood_pressure/any_event/systolic": "160",
        "vital_signs/blood_pressure/any_event/diastolic": "100",
        },
	    2: {
        "ctx/language": "en",
	    "ctx/territory": "SI",
	    "ctx/time": "2014-02-27T14:37:43.000+01:00",
	    "vital_signs/blood_pressure/any_event/systolic": "180",
        "vital_signs/blood_pressure/any_event/diastolic": "120",
        },
	    3:  {
        "ctx/language": "en",
	    "ctx/territory": "SI",
	    "ctx/time": "2014-02-27T01:35:11.000+01:00",
	    "vital_signs/blood_pressure/any_event/systolic": "172",
        "vital_signs/blood_pressure/any_event/diastolic": "131",
        },
        4:  {
        "ctx/language": "en",
	    "ctx/territory": "SI",
	    "ctx/time": "2014-02-25T21:17:11.000+01:00",
	    "vital_signs/blood_pressure/any_event/systolic": "194",
        "vital_signs/blood_pressure/any_event/diastolic": "133",
        },
        5:  {
        "ctx/language": "en",
	    "ctx/territory": "SI",
	    "ctx/time": "2014-02-06T08:26:28.000+01:00",
	    "vital_signs/blood_pressure/any_event/systolic": "183",
        "vital_signs/blood_pressure/any_event/diastolic": "144",
        },
    }

},
2: {
        data: {
        firstNames: "Slavko",
        lastNames: "Zdravko",
        dateOfBirth: "1953-02-10T01:30",
    },
    
    party: {                    		    
                1: {
        "ctx/language": "en",
	    "ctx/territory": "SI",
	    "ctx/time": "2014-03-03T07:28:33.000+01:00",
	    "vital_signs/blood_pressure/any_event/systolic": "150",
        "vital_signs/blood_pressure/any_event/diastolic": "100",
        },
	    2: {
        "ctx/language": "en",
	    "ctx/territory": "SI",
	    "ctx/time": "2014-02-27T14:37:43.000+01:00",
	    "vital_signs/blood_pressure/any_event/systolic": "145",
        "vital_signs/blood_pressure/any_event/diastolic": "91",
        },
	    3:  {
        "ctx/language": "en",
	    "ctx/territory": "SI",
	    "ctx/time": "2014-02-27T01:35:11.000+01:00",
	    "vital_signs/blood_pressure/any_event/systolic": "155",
        "vital_signs/blood_pressure/any_event/diastolic": "98",
        },
        4:  {
        "ctx/language": "en",
	    "ctx/territory": "SI",
	    "ctx/time": "2014-02-25T21:17:11.000+01:00",
	    "vital_signs/blood_pressure/any_event/systolic": "153",
        "vital_signs/blood_pressure/any_event/diastolic": "104",
        },
        5:  {
        "ctx/language": "en",
	    "ctx/territory": "SI",
	    "ctx/time": "2014-02-06T08:26:28.000+01:00",
	    "vital_signs/blood_pressure/any_event/systolic": "145",
        "vital_signs/blood_pressure/any_event/diastolic": "111",
        },
    }

},
3: {
        data: {
        firstNames: "Danko",
        lastNames: "Normalko",
        dateOfBirth: "1990-07-10T09:30",
    },
    
    party: {                    		    
        1: {
        "ctx/language": "en",
	    "ctx/territory": "SI",
	    "ctx/time": "2014-03-03T07:28:33.000+01:00",
	    "vital_signs/blood_pressure/any_event/systolic": "120",
        "vital_signs/blood_pressure/any_event/diastolic": "80",
        },
	    2: {
        "ctx/language": "en",
	    "ctx/territory": "SI",
	    "ctx/time": "2014-02-27T14:37:43.000+01:00",
	    "vital_signs/blood_pressure/any_event/systolic": "125",
        "vital_signs/blood_pressure/any_event/diastolic": "84",
        },
	    3:  {
        "ctx/language": "en",
	    "ctx/territory": "SI",
	    "ctx/time": "2014-02-27T01:35:11.000+01:00",
	    "vital_signs/blood_pressure/any_event/systolic": "130",
        "vital_signs/blood_pressure/any_event/diastolic": "82",
        },
        4:  {
        "ctx/language": "en",
	    "ctx/territory": "SI",
	    "ctx/time": "2014-02-25T21:17:11.000+01:00",
	    "vital_signs/blood_pressure/any_event/systolic": "121",
        "vital_signs/blood_pressure/any_event/diastolic": "84",
        },
        5:  {
        "ctx/language": "en",
	    "ctx/territory": "SI",
	    "ctx/time": "2014-02-06T08:26:28.000+01:00",
	    "vital_signs/blood_pressure/any_event/systolic": "131",
        "vital_signs/blood_pressure/any_event/diastolic": "72",
        },
    }

}
};

