
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";


/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}

function addPatientParty(sessionId, params, patientData){

	for(var i in patientData){
	    $.ajax({
    	    headers: {"Ehr-Session": sessionId},
    	    url: baseUrl + "/composition?" + $.param(params),
    	    type: 'POST',
    	    async: false,
    	    contentType: 'application/json',
    	    data: JSON.stringify(patientData[i]),
    	    success: function (res) {
    	        console.log("Post patient data");
    	    },
    	    
    	    error: function(napaka){
    	        console.log(napaka);
    	    }
	    });
	}
}

function addPatient(sessionId, ehrId, patient){
    
 patient.data["partyAdditionalInfo"] = [{key: "ehrId", value: ehrId}];    
 $.ajax({
    url: baseUrl + "/demographics/party",
    type: 'POST',
    async: false,
    headers: {"Ehr-Session": sessionId},
    contentType: 'application/json',
    data: JSON.stringify(patient.data),
    success: function (party) {
        var params = {
    	    ehrId: ehrId,
    	    templateId: 'Vital Signs',
    	    format: 'FLAT',
    	};
    	
    	addPatientParty(sessionId, params, patient.party);
    },
    error: function (error) {
        console.log(error);
    }});

}

function getEhrId(sessionId){
    var ehrId;
    
    $.ajax({
    url: baseUrl + "/ehr",
    type: 'POST',
    headers: {"Ehr-Session": sessionId},
    async: false,
    success: function (data) {
        ehrId = data.ehrId;
    },
    error: function (error) {
        console.log(error);
    }
    });
    
    return ehrId;
}


function preberiUporabnika(){
	var id= $('#ehrKljuc').val();
	var sessionId = getSessionId();

	$.ajax({
	    url: baseUrl + "/view/" + id + "/blood_pressure",
	    type: 'GET',
	    headers: {"Ehr-Session": sessionId},
	    success: function (res) {
	        // "diastolic", "systolic", "unit"
	        drawGraphBloodPressure(res);
	    	console.log(res);
	    },
	    error: function() {
	    	console.log("error");
	    }
	});
		
			
		
		
	
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke(stPacienta) {
  ehrId = "";
  var sessionId = getSessionId();
  ehrId = getEhrId(sessionId);
  
  addPatient(sessionId, ehrId, pacienti[stPacienta]);
  $("#ehrKljuc").val(ehrId);
  
  return ehrId;
}


function drawGraphBloodPressure(res){
    console.log(res);
    res.forEach(function (el, i, arr) {
        var date = new Date(el.time);
        el.date = date.getTime();
    });

    $('#pritisk').html("");
    new Morris.Area({
        element: 'pritisk',
        data: res.reverse(),
        xkey: 'date',
        ykeys: ['diastolic', 'systolic'],
        lineColors: ['#2c8e2c', '#dae843'],
        labels: ['Diastolic', "Systolic"],
        lineWidth: 2,
        pointSize: 3,
        hideHover: true,
        behaveLikeLine: true,
        smooth: false,
        resize: true,
        xLabels: "day",
        xLabelFormat: function (x) {
            var date = new Date(x);
            return (date.getDate() + '-' + monthNames[date.getMonth()]);
        },
        dateFormat: function (x) {
            return (formatDate(x, false));
        }
    });

    
    var down = res[res.length - 1].diastolic;
    var up = res[res.length - 1].systolic;
    
    
    $('#zgornja').text(up + " " + res[res.length - 1].unit);
    $('#spodnja').text(down + " " + res[res.length - 1].unit);
    $('#verdict').text(analizaKrvnegaTlaka(up, down));
    
    
    $('#last').unbind("click");
    $('#last').on("click", function(){
        $("#analiza").toggle();
        if( $("#analiza").is(':visible')){
        $("#kliknite").text("Skrij");
        }else{
        $("#kliknite").text("Zadnja meritvev");
        }
    });
    
}


function analizaKrvnegaTlaka(sys, dia){
    var systolic = sys;
    var diastolic = dia;
    if ((systolic < 80) && (diastolic < 50)) {
        return 'Zelo nizek krvni pritisk. Prosim posvetujte se s svojim zdravnikom in preverite simptome niskega krvnega pritiska'
    } else if ((systolic < 91) && (diastolic < 65)) {
        return 'Zelo nizek krvni pritisk! Poiščite načine kako zvišati krvni pritisk ali pa se posvetujte z zdravnikom.'
    } else if ((systolic < 101) && (diastolic < 71)) {
        return 'Nizek krvni pritisk.'
    } else if ((systolic < 121) && (diastolic < 81)) {
        return "Normalen krvni pritisk!"
    } else if ((systolic < 130) && (diastolic < 86)) {
        return "Rahlo zvišan krvni pritisk."
    } else if ((systolic < 140) && (diastolic < 90)) {
        return "Nekoliko povečan!."
    } else if ((systolic < 160) && (diastolic < 100)) {
        return 'Zmerno visok krvni pritisk! Možnost hipertenzije. Prosim posvetujte se s svojim zdravnikom.'
    } else if ((systolic < 180) && (diastolic < 110)) {
        return "Visok krvni pritisk! Visok krvni pritisk! Možnost hipertenzije. Prosim posvetujte se s svojim zdravnikom."
    } else if ((systolic < 250) && (diastolic < 190)) {
        return "Zelo visok krvni pritisk! Visoka vrjetnost hipertenzije. Takoj se posvetujte z zdravnikom."
    }
}

var monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];




function formatDate(date, completeDate) {

    var d = new Date(date);

    var curr_date = d.getDate();
    curr_date = normalizeDate(curr_date);

    var curr_month = d.getMonth();
    curr_month++;
    curr_month = normalizeDate(curr_month);

    var curr_year = d.getFullYear();

    var curr_hour = d.getHours();
    curr_hour = normalizeDate(curr_hour);

    var curr_min = d.getMinutes();
    curr_min = normalizeDate(curr_min);

    var curr_sec = d.getSeconds();
    curr_sec = normalizeDate(curr_sec);

    var dateString, monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

    if (completeDate){
        dateString = curr_date + "-" + monthNames[curr_month-1] + "-" + curr_year + " at " + curr_hour + ":" + curr_min; // + ":" + curr_sec;
    }
    else dateString = curr_date + "-" + monthNames[curr_month-1] + "-" + curr_year;

    return dateString;

}

function normalizeDate(el) {
    el = el + "";
    if (el.length == 1) {
        el = "0" + el;
    }
    return el;
}


var map;
var service;
var infowindow;

function getUserPos() {

    $("#lokacija").text("");
    navigator.geolocation.getCurrentPosition(function(position) {
    
      // Get the coordinates of the current possition.
      var lat = position.coords.latitude;
      var lng = position.coords.longitude;

      console.log("lat:" + lat + " lng:" + lng)
      
      showHosp(lat, lng);
    }, function(){
        
        var lat = 46.056947;
        var lng = 14.505751;
        
        $("#lokacija").text("Vaše lokacije ni bilo mogoče najti. Privzeta lokacija Ljubljana");
        showHosp(lat, lng);
        
        
    });
    
 
}

function showHosp(lat, lng){

      console.log("lat:" + lat + " lng:" + lng)
         
      var pyrmont = new google.maps.LatLng(lat, lng);
    
      map = new google.maps.Map(document.getElementById('map'), {
          center: pyrmont,
          zoom: 13
        });
    
      var request = {
        location: pyrmont,
        radius: '5000',
        types: ['hospital']
      };
    
      infowindow = new google.maps.InfoWindow();
      service = new google.maps.places.PlacesService(map);
      service.nearbySearch(request, callback);
}

function callback(results, status) {
  if (status == google.maps.places.PlacesServiceStatus.OK) {
    for (var i = 0; i < results.length; i++) {
      var place = results[i];
      createMarker(results[i]);
    }
  }
}

function createMarker(place) {
    var placeLoc = place.geometry.location;
    var marker = new google.maps.Marker({
      map: map,
      position: place.geometry.location
    });
    
    google.maps.event.addListener(marker, 'click', function() {
      infowindow.setContent(place.name);
      infowindow.open(map, this);
    });
}


